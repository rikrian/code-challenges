from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from .api import list_users, create_user, delete_user, create_iou, get_user


def list_users_view(request):
    """List Users """
    data = list_users()
    return HttpResponse(data, status=200)


def get_user_view(request, username):
    data = get_user(username)
    return HttpResponse(data, status=200)


@csrf_exempt
def create_user_view(request):
    """Deletes a user """
    data = create_user(request.POST.dict())
    return HttpResponse(data, status=201)


@csrf_exempt
def delete_user_view(request):
    """Deletes a users and who over owes them """
    result = delete_user(username=request.POST.dict().get("username"))
    return HttpResponse(status=204)


@csrf_exempt
def create_iou_view(request):
    data = create_iou(request.POST.dict())
    return HttpResponse(data, status=201)
