# Generated by Django 3.1 on 2020-08-27 13:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("users_api", "0002_iou"),
    ]

    operations = [
        migrations.RenameField(
            model_name="iou",
            old_name="borrowers",
            new_name="borrower",
        ),
    ]
