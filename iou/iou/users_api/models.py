from django.db import models


class IouUser(models.Model):
    username = models.CharField(max_length=8, help_text="The username of the user")

    def get_owed_by(self):
        owed_by = self.borrowers.all()
        return [iou.borrower.username for iou in owed_by]

    def get_owes(self):
        owes = self.lenders.all()
        return [iou.lender.username for iou in owes]

    def get_balance(self):
        owes = self.lenders.all()
        owed_by = self.borrowers.all()
        owes_amount = sum([float(owe.amount) for owe in owes])
        owed_amount = sum([float(owe.amount) for owe in owed_by])
        return owes_amount - owed_amount

    def to_dict(self):
        return {
            "name": self.username,
            "owes": self.get_owes(),
            "owed_by": self.get_owed_by(),
            "balance": self.get_balance(),
        }


class IOU(models.Model):
    lender = models.ForeignKey(
        IouUser, related_name="borrowers", on_delete=models.CASCADE
    )
    borrower = models.ForeignKey(
        IouUser, related_name="lenders", on_delete=models.CASCADE
    )
    amount = models.FloatField()
