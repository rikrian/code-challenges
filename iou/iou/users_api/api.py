import json

from .models import IouUser, IOU


class APIException(Exception):
    pass


def list_users():
    users = [user.username for user in IouUser.objects.all()]
    return json.dumps({"users": users})


def create_user(data):
    username = data.get("username").lower()
    try:
        user = IouUser.objects.create(username=username)
    except:
        raise APIException("An error occured")
    return json.dumps({"username": user.username})


def get_user(username):
    user = IouUser.objects.get(username=username)
    return json.dumps(user.to_dict())


def delete_user(username):
    try:
        user = IouUser.objects.get(username=username)
        user.delete()
    except IouUser.DoesNotExist:
        return APIException("The user with username, does not exist".format(username))


def create_iou(data):
    """
    Creates a single IOU record.

    Expects data in the format below:
        {
            "lender": 'Test1',
            "borrower": "Test2".
            "amount": 10.50
        }
    """
    try:
        lender = IouUser.objects.get(username=data.get("lender").lower())
        data["lender"] = lender
    except IouUser.DoesNotExist:
        raise APIException("The Lender Does Not exist")

    try:
        borrower = IouUser.objects.get(username=data.get("borrower").lower())
        data["borrower"] = borrower
    except IouUser.DoesNotExist:
        raise APIException("The Borrower Does Not exist")
    IOU.objects.create(**data)
    return list_users()
