from django.urls import path

from users_api.views import (
    list_users_view,
    create_user_view,
    delete_user_view,
    create_iou_view,
    get_user_view,
)

urlpatterns = [
    path("users", list_users_view, name="list_users"),
    path("user/<username>", get_user_view, name="user_details"),
    path("add", create_user_view, name="create_user"),
    path("iou", create_iou_view, name="create_iou"),
]
