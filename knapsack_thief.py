"""
Get the items that a thief can place in his knapsack for max value.

Uses dynamic programming approach.
"""


def max_value():
    items = [
        {"weight": 5, "value": 10},
        {"weight": 4, "value": 40},
        {"weight": 6, "value": 30},
        {"weight": 4, "value": 40},
    ]
    item_weights = [item.get("weight") for item in items]
    item_values = [item.get("value") for item in items]

    n = len(item_weights)
    W = 10  # capacity of the knapsnack
    K = [[0 for w in range(W + 1)] for i in range(n)]

    for i in range(1, n):
        for cur_weight in range(1, W + 1):
            wi = item_weights[i]
            vi = item_values[i]

            if wi <= cur_weight:
                K[i][cur_weight] = max(
                    [K[i - 1][cur_weight - wi] + vi, K[i - 1][cur_weight]]
                )
            else:
                K[i][cur_weight] = K[i - 1][cur_weight]

    return K[n - 1][W]
