"""
A darts game scorer. 
"""
import math


def check_if_point_in_circle(xc, yc, xp, yp, radius):
    """
    Given a circle with center xc, yc and the radius,

    determine if the given points xp and yp are inside the circle or fall on the circumference.

    Formula to use d=\sqrt{(x_p-x_c)^2+(y_p-y_c)^2}\;.
    if d < radius then point is inside the circle, d == r the point is on the circumference
    if d > radious the point is outside the circle
    """
    d = math.sqrt((xp - xc) ** 2 + (yp - yc ** 2))
    if d < radius or d == radius:
        return True
    else:
        return False


def score_throws(xc, yc, xp=1, yp=1, radius=2):
    radius_scores = {
        "radius_1": 1,
        "score": 10,
        "radius_5": 5,
        "score": 5,
        "radius_10": 10,
        "score": 1,
    }

    score = 0
    radius_seen = False

    if check_if_point_in_circle(xc, yc, xp, yp, radius_scores.get("radius_1")):
        score = 10

    elif check_if_point_in_circle(xc, yc, xp, yp, radius_scores.get("radius_5")):
        score = 5

    elif check_if_point_in_circle(xc, yc, xp, yp, radius_scores.get("radius_10")):
        score = 1
    else:
        score = 0

    return score


def driver():
    """Servers as a test to ensure that the code works."""
    result_0 = score_throws(xc=0, yc=0, xp=1, yp=0, radius=1)
    result_1 = score_throws(xc=0, yc=0, xp=1, yp=1, radius=1)
    result_2 = score_throws(xc=0, yc=0, xp=3, yp=4, radius=5)
    result_3 = score_throws(xc=0, yc=0, xp=6, yp=7, radius=10)

    assert result_0 == 10  # point is on the circumfrence of the inner circel
    assert result_1 == 5  # point is outside the mid circle but outside inner circe
    assert result_2 == 5
    assert result_3 == 1
