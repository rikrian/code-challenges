import React, { Component} from 'react';
import axios from  'axios';


class  Books extends Component{

   constructor(props){
        super(props);
        this.state = {
            books: [],
        };
    }
    

    getBooks(){
       axios.get('https://anapioficeandfire.com/api/books/',
                {
                    headers:{
                        'Content-Type': 'application/json',
                        "X-Requested-With": "XMLHttpRequest"
                    }
                }
        ).then(
            res => {
                this.setState({books: res.data});
                console.log(res.data);
            }
        )
    }   

  componentDidMount() {
       this.getBooks();
   }


  render() {
      return  (
            <div>
                    <h3>Books</h3>
                    <table className='table'>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Authors</th>
                            <th>ISBN</th>
                            <th>Number of Pages</th>
                            <th>Country</th>
                            <th>Publisher</th>
                        </tr>
                           { this.state.books.map( book =>
                                <tr>
                                        <td> <a href={ book.url }  target='_blank'> { book.url }</a></td>
                                        <td> { book.name }</td>
                                        <td> { book.authors.map( author => <p> { author }</p>) }</td>
                                        <td> { book.isbn }</td>
                                        <td> { book.numberOfPages }</td>
                                        <td> { book.country }</td>
                                        <td> { book.publisher }</td>
                                 </tr>
                         
                            )}
                    </table>
            </div>
      );
    }
}

export default Books;
