import React, { Component} from 'react';
import axios from  'axios';


class  Houses extends Component{

   constructor(props){
        super(props);
        this.state = {
            houses: [],
        };
    }
    

    getHouses(){
       axios.get('https://anapioficeandfire.com/api/houses/',
                {
                    headers:{
                        'Content-Type': 'application/json',
                        "X-Requested-With": "XMLHttpRequest"
                    }
                }
        ).then(
            res => {
                this.setState({houses: res.data});
                console.log(res.data);
            }
        )
    }   

  componentDidMount() {
       this.getHouses();
   }


  render() {
      return  (
            <div>
                    <h3>Houses</h3>
                    <table className='table'>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Region</th>
                            <th>Words</th>
                        </tr>
                           { this.state.houses.map( house =>
                                <tr>
                                        <td> <a href={ house.url } target='_blank'> { house.url }</a></td>
                                        <td> { house.name }</td>
                                        <td> { house.region }</td>
                                        <td> { house.words }</td>
                                 </tr>
                         
                            )}
                    </table>
            </div>
      );
    }
}

export default Houses;
