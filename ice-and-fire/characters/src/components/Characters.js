import React, { Component} from 'react';
import axios from  'axios';


class  Characters extends Component{

   constructor(props){
        super(props);
        this.state = {
            characters: [],
        };
    }
    

    getCharacters(){
       axios.get('https://anapioficeandfire.com/api/characters/',
                {
                    headers:{
                        'Content-Type': 'application/json',
                        "X-Requested-With": "XMLHttpRequest"
                    }
                }
        ).then(
            res => {
                this.setState({characters: res.data});
                console.log(res.data);
            }
        )
    }   

  componentDidMount() {
       this.getCharacters();
   }


  render() {
      return  (
            <div>
                    <h3>Characters</h3>
                    <table className='table'>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Culture</th>
                        </tr>
                           { this.state.characters.map( character =>
                                <tr>
                                        <td> <a href={ character.url } target='_blank'> {character.url }</a></td>
                                        <td> { character.name }</td>
                                        <td> { character.gender }</td>
                                        <td> { character.culture }</td>
                                 </tr>
                         
                            )}
                    </table>
            </div>
      );
    }
}

export default Characters;
