import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";


import  Characters  from './components/Characters';
import  Houses  from './components/Houses';
import  Books  from './components/Books';


function App() {
  return (
    <div className="App">
         <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Characters</Link>
          </li>
          <li>
            <Link to="/houses">Houses</Link>
          </li>
          <li>
            <Link to="/books">Books</Link>
          </li>
        </ul>

        <hr />

        {/*
          A <Switch> looks through all its children <Route>
          elements and renders the first one whose path
          matches the current URL. Use a <Switch> any time
          you have multiple routes, but you want only one
          of them to render at a time
        */}
        <Switch>
          <Route exact path="/">
            <Characters />
          </Route>
          <Route path="/houses">
            <Houses />
          </Route>
          <Route path="/books">
            <Books />
          </Route>
        </Switch>
      </div>
    </Router>
    </div>
  );
}

export default App;
